package com.energizeglobal.interpersonal.entity;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;

@Entity
@Table(name = "employee", indexes = {@Index(name = "employee_birthday_IDX", columnList = "birthday")})
@Data
public class EmployeeEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "email", unique = true, updatable = false, nullable = false)
    @Email
    private String email;

    @Column(name = "password", nullable = false)
    @NotNull
    private String password;

    @Column(name = "birthday", nullable = false)
    @NotNull
    private LocalDate birthday;

    @Column(name = "photo_path", length = 1000)
    private String photoPath;

    @Column(name = "mobile_number", unique = true, length = 50)
    private String mobileNumber;

    @Column(name = "hobbies", length = 1000)
    private String hobbies;

    @Column(name = "phobias", length = 1000)
    private String phobias;

    @Column(name = "additional_info", length = 1000)
    private String additionalInfo;
}
