package com.energizeglobal.interpersonal.entity;

import com.energizeglobal.interpersonal.entity.composite_key.EmployeeRoomId;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "employee_room", indexes =
        {
                @Index(name = "employee_id_IDX", columnList = "employee_id"),
                @Index(name = "room_id_IDX", columnList = "room_id")
        })
@Data
public class EmployeeRoomEntity implements Serializable {

    @EmbeddedId
    private EmployeeRoomId employeeRoomId;

    @Column(name = "is_accept_notifications", nullable = false)
    private Boolean isAcceptNotifications = Boolean.TRUE;

    @Column(name = "is_send_notifications", nullable = false)
    private Boolean isSendNotifications = Boolean.TRUE;

}
