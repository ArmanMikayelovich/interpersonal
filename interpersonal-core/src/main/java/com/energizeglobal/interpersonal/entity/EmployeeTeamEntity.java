package com.energizeglobal.interpersonal.entity;

import com.energizeglobal.interpersonal.entity.composite_key.EmployeeTeamId;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "employee_team", indexes = {
        @Index(name = "employee_id_IDX", columnList = "employee_id"),
        @Index(name = "team_id_IDX", columnList = "team_id"),
})
@Data
public class EmployeeTeamEntity implements Serializable {
    @EmbeddedId
    private EmployeeTeamId employeeTeamId;

    @Column(name = "is_accept_notifications", nullable = false)
    private Boolean isAcceptNotifications = Boolean.TRUE;

    @Column(name = "is_send_notifications", nullable = false)
    private Boolean isSendNotifications = Boolean.TRUE;
}
