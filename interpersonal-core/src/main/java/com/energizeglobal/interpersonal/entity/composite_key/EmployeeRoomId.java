package com.energizeglobal.interpersonal.entity.composite_key;

import com.energizeglobal.interpersonal.entity.EmployeeEntity;
import com.energizeglobal.interpersonal.entity.RoomEntity;
import lombok.Data;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.io.Serializable;

@Embeddable
@Data
@Table(name = "employee_room")
public class EmployeeRoomId implements Serializable {

    @OneToOne
    @JoinColumn(name = "employee_id", referencedColumnName = "id", nullable = false,table = "employee_room")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private EmployeeEntity employeeEntity;

    @ManyToOne
    @JoinColumn(name = "room_id", referencedColumnName = "id", nullable = false,table = "employee_room")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private RoomEntity roomEntity;
}
