package com.energizeglobal.interpersonal.entity.composite_key;

import com.energizeglobal.interpersonal.entity.EmployeeEntity;
import com.energizeglobal.interpersonal.entity.TeamEntity;
import lombok.Data;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;

@Embeddable
@Data
@Table(name = "employee_team")
public class EmployeeTeamId implements Serializable {
    @ManyToOne
    @JoinColumn(name = "employee_id", referencedColumnName = "id", nullable = false,table = "employee_team")
    private EmployeeEntity employeeEntity;
    @ManyToOne
    @JoinColumn(name = "team_id", referencedColumnName = "id", nullable = false,table = "employee_team")
    private TeamEntity teamEntity;
}
